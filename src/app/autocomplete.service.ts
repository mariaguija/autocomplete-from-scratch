import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AutocompleteService {

  public query: string = '';
  public data: any = [];
  public filteredList: any = [];

  constructor(public http: HttpClient) { }

  search() {
    if (this.query !== '') {
      this.filteredList = this.data.filter((item) => {
        return item.name.toLowerCase().indexOf(this.query.toLocaleLowerCase()) > -1;
      });

    } else {
      this.filteredList = [];
    }
  }

  select(item) {
    this.query = item.name;
    this.filteredList = [];
  }

  getData() {
    this.http.get('./assets/res/countries.json')
      .subscribe(data => {
        this.data = data;
      });
  }
}
