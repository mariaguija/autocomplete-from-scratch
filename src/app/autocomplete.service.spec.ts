import {TestBed, async, inject} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {MockBackend} from '@angular/http/testing';
import {ResponseOptions, Response} from '@angular/http';
import {AutocompleteService} from './autocomplete.service';

describe('AutocompleteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AutocompleteService, MockBackend]
    });
  });
  it('should be created', async(inject([HttpTestingController, AutocompleteService],
    (httpClient: HttpTestingController, service: AutocompleteService) => {
      expect(service).toBeTruthy();
  })));
  it('should return an countries array',
    inject([AutocompleteService, MockBackend], (service, mockBackend) => {
      const mockResponse = [{
        'name': 'Georgia',
        'code': 'GE',
        'flag': './assets/img/ge.png'
      }, {
        'name': 'Germany',
        'code': 'DE',
        'flag': './assets/img/de.png'
      }, {
        'name': 'Ghana',
        'code': 'GH',
        'flag': './assets/img/gh.png'
      }, {
        'name': 'Gibraltar',
        'code': 'GI',
        'flag': './assets/img/gi.png'
      }];
      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });
      service.data = mockResponse;
      service.query = 'Germany';
      service.search();
      expect(service.filteredList[0].name).toBe('Germany');
      expect(service.getData).toBeTruthy();
  }));
});
