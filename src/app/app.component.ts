import {Component, OnInit} from '@angular/core';
import {AutocompleteService} from './autocomplete.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public service: AutocompleteService) { }

  ngOnInit() {
    this.service.getData();
  }
}
